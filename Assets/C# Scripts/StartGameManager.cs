using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartGameManager : MonoBehaviour
{
    [SerializeField] private WatcherFood Food;
    [SerializeField] private WatcherPeople People;
    [SerializeField] private WatcherWarriors Warriors;
    [SerializeField] private GameTimer Timer;
    [SerializeField] private BattleManager Battle;

    public void StartGame()
    {
        Time.timeScale = 1;
        Timer.time = 60;
        Timer.timerText.text = Timer.time.ToString();

        Food.numFood = 3;
        Food.swapAnimationFood = false;
        Food.numFoodTxt.text = Food.numFood.ToString();
        Food.timer = 3;

        People.numPeople = 3;
        People.swapAnimationPeople = false;
        People.numPeopleTxt.text = People.numPeople.ToString();
        People.timer = 4;

        Warriors.numWarriors = 0;
        Warriors.swapAnimationWarriors = false;
        Warriors.numWarriorsTxt.text = Warriors.numWarriors.ToString();
        Warriors.timer = 7;

        Battle.numEnemies = 1;
        Battle.txtEnemies.text = Battle.numEnemies.ToString();
        Battle.numWave = 1;
        Battle.numLastWave = 0;
        Battle.txtWave.text = Battle.numWave.ToString();
    }
}
