using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameButtonManager : MonoBehaviour
{
    public WatcherFood food;
    public WatcherWarriors warriors;
    public WatcherPeople people;

    private void Update()
    {
        NumberOfPeople();
        UpdatePeople();
        NumberOfFood();
        UpdateOfFood();
        NumberOfWarriors();
        UpdateOfWarriors();
    }
    void NumberOfPeople()
    {
        if (people.activePlusPeople == true)
        {
            people.buttonPlusPeople.interactable = false;
            people.buttonUpdatePeople.interactable = false;
        }
        if (people.activePlusPeople == false && food.numFood > 0)
        {
            people.buttonPlusPeople.interactable = true;
        }
        if (food.numFood <= 0)
        {
            people.buttonPlusPeople.interactable = false;
        }
    }
    void UpdatePeople()
    {
        if (people.swapAnimationPeople == false)
        {
            if (people.buttonPlusPeople.interactable== true && food.numFood >= 5)
            {
                people.buttonUpdatePeople.interactable = true;
            }
        }
        if (food.numFood < 5)
        {
            people.buttonUpdatePeople.interactable = false;
        }
        if(people.swapAnimationPeople ==true)
        {
            people.buttonUpdatePeople.interactable = false;
        }
    }
    void NumberOfFood()
    {
            if (food.activePlusFood == true)
        {
            food.buttonPlusFood.interactable = false;
            food.buttonUpdateFood.interactable = false;
        }
        if (food.activePlusFood == false)
        {
            if(people.numPeople > 0)
            {
                food.buttonPlusFood.interactable = true;
            }
        }
        if (people.numPeople <= 0)
        {
            food.buttonPlusFood.interactable = false;
        }
    }
    void UpdateOfFood()
    {
        if (food.swapAnimationFood == false)
        {
            if (food.buttonPlusFood.interactable == true && people.numPeople >= 4)
            {
                food.buttonUpdateFood.interactable = true;
            }
        }
        if (people.numPeople < 4)
        {
            food.buttonUpdateFood.interactable = false;
        }
        if (food.swapAnimationFood == true)
        {
            food.buttonUpdateFood.interactable = false;
        }
    }
    void NumberOfWarriors()
    {
        if (warriors.activePlusWarriors == true)
        {
            warriors.buttonPlusWarriors.interactable = false;
            warriors.buttonUpdateWarriors.interactable = false;
        }
        if ( people.numPeople >= 1 && food.numFood >=2)
        {
            if (warriors.activePlusWarriors == false)
            {
                warriors.buttonPlusWarriors.interactable = true;
            }
        }

        if (people.numPeople < 1)
        {
            warriors.buttonPlusWarriors.interactable = false;
        }
        if (food.numFood < 2)
        {
            warriors.buttonPlusWarriors.interactable = false;
        }
    }
    void UpdateOfWarriors()
    {
        if (warriors.swapAnimationWarriors == false)
        {
            
                if (people.numPeople < 2)
                {
                    warriors.buttonUpdateWarriors.interactable = false;
                }
                if (food.numFood < 4)
                {
                    warriors.buttonUpdateWarriors.interactable = false;
                }
                if (people.numPeople >= 2)
                {
                    if (food.numFood >= 4)
                    {
                        warriors.buttonUpdateWarriors.interactable = true;
                    }
                }
        }
        if (warriors.swapAnimationWarriors == true)
        {
            warriors.buttonUpdateWarriors.interactable = false;
        }
    }
}