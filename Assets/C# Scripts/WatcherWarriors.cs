using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class WatcherWarriors : MonoBehaviour
{
    public WatcherFood food;
    public WatcherPeople people;
    public AudioSource wacherSourse;
    private Animator animator;
    public Button buttonPlusWarriors;
    public Button buttonUpdateWarriors;
    [NonSerialized] public bool activePlusWarriors;
    [NonSerialized] public bool swapAnimationWarriors;
    [NonSerialized] public float timer = 7f;
    [NonSerialized] public float numWarriors;
    public Text numWarriorsTxt;
    void Start()
    {
        animator = GetComponent<Animator>();
        swapAnimationWarriors = false;
    }

    private void FixedUpdate()
    {
        WarriorsManager();
    }
    public void WarriorsManager()
    {
        if (swapAnimationWarriors == false)
        {
            if (activePlusWarriors)
            {
                animator.SetBool("boolWarrior", true);
                timer -= Time.fixedDeltaTime;
                buttonPlusWarriors.interactable = false;
                if (timer <= 0)
                {
                    wacherSourse.Stop();
                    activePlusWarriors = false;
                    animator.SetBool("boolWarrior", false);
                    food.numFoodTxt.text = food.numFood.ToString();
                    timer = 7f;
                    buttonPlusWarriors.interactable = true;
                    numWarriors += 1;
                    numWarriorsTxt.text = numWarriors.ToString();
                }
            }
        }
        if (swapAnimationWarriors == true)
        {
            if (timer > 5f)
            {
                timer = 5f;
            }
            if (activePlusWarriors)
            {
                animator.SetBool("boolUpdateWarriors", true);
                timer -= Time.fixedDeltaTime;
                buttonPlusWarriors.interactable = false;
                if (timer <= 0)
                {
                    activePlusWarriors = false;
                    wacherSourse.Stop();
                    animator.SetBool("boolUpdateWarriors", false);
                    timer = 5f;
                    numWarriors += 1;
                    numWarriorsTxt.text = numWarriors.ToString();
                    buttonPlusWarriors.interactable = true;
                }
            }
        }
    }
    public void ClickPlusWarriors()
    {
        wacherSourse.Play();
        food.numFood -= 2;
        people.numPeople -= 1;
        food.numFoodTxt.text = food.numFood.ToString();
        people.numPeopleTxt.text = people.numPeople.ToString();
        activePlusWarriors = true;
    }

    public void ClickUpdateWarriors()
    {
        buttonUpdateWarriors.interactable = false;
        swapAnimationWarriors = true;
        food.numFood -= 4;
        people.numPeople -= 2;
        food.numFoodTxt.text = food.numFood.ToString();
    }
}