using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
     public float time = 60;
     public Text timerText;

    private void FixedUpdate()
    {
        Timer();
    }
    void Timer()
    {
        if (time>0)
        {
            time -= Time.deltaTime;
            timerText.text = Mathf.Round(time).ToString();
        }
    }
}