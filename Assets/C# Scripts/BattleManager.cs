using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;


public class BattleManager : MonoBehaviour
{
   public GameTimer timer;
   public WatcherWarriors warriors;

   public Canvas defeatCanvas;
   public Canvas gameCanvas;
   public Text txtWave;
   [NonSerialized] public int numWave = 1;
   [NonSerialized] public int numLastWave = 0;
   public Text txtEnemies;
   [NonSerialized] public float numEnemies = 1f;
   [NonSerialized] public float battleTime =5;
   public AudioSource defeatGame;

    private void Start()
    {
        txtWave.text = numWave.ToString();
        txtEnemies.text = numEnemies.ToString();
    }
    private void Update()
    {
        WaveManager();
    }
    void WaveManager()
    {
        if(timer.time <=0)
        {
            if (warriors.numWarriors <= 0)
            {
                warriors.numWarriors = 0;
                defeatGame.Play();
                defeatCanvas.gameObject.SetActive(true);
                gameCanvas.gameObject.SetActive(false);
            }
            if (warriors.numWarriors >0)
            {
                battleTime += (Time.deltaTime)*100;
                timer.time = 0;
                if (battleTime > 0)
                {
                    warriors.numWarriors -= UnityEngine.Random.Range(1, 2);
                    numEnemies -= UnityEngine.Random.Range(1, 2);
                    warriors.numWarriorsTxt.text = warriors.numWarriors.ToString();
                    txtEnemies.text = numEnemies.ToString();
                }
            }
        }
        if (numEnemies <= 0)
        {
            numEnemies = 0;
            timer.time = 60;
            numLastWave += 1;
        }
        if (numLastWave == numWave)
        {
            numWave += 1;
            numEnemies = numWave * 2f;
            txtEnemies.text = numEnemies.ToString();
            txtWave.text = numWave.ToString();
        }
    }
}
