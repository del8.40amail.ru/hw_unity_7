using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseCanvas : MonoBehaviour
{
    [SerializeField] private Button continueBatton;
    [SerializeField] private Button exitMenuBatton;
    [SerializeField] private AudioSource gameMusic;
    [SerializeField] private AudioSource gameSoundFier;
    [SerializeField] private AudioSource gameSoundMenu;
    public void ClickButtonPause()
    {
        Time.timeScale= 0;
    }
    public void ClickButtonContinueGame()
    {
        Time.timeScale = 1;
    }

    public void ClickOptionInPause()
    {
        continueBatton.gameObject.SetActive(true);
        exitMenuBatton.gameObject.SetActive(false);
    }
    public void ClickContinueInPause()
    {
        continueBatton.gameObject.SetActive(false);
        exitMenuBatton.gameObject.SetActive(true);
    }
    public void ClickMenuInPause()
    {
        gameMusic.Stop();
        gameSoundFier.Stop();
        gameSoundMenu.Play();
    }
}
