using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class WatcherPeople : MonoBehaviour
{
    public WatcherFood food;
    public WatcherWarriors warriors;
    public AudioSource wacherSourse;
    private Animator animator;
    public Button buttonPlusPeople;
    public Button buttonUpdatePeople;
    [NonSerialized] public bool swapAnimationPeople;
    [NonSerialized] public bool activePlusPeople;
    [NonSerialized] public float timer=4f;
    public Text numPeopleTxt;
    [NonSerialized] public float numPeople = 3f;
    void Start()
    {
        animator = GetComponent<Animator>();
        numPeopleTxt.text = numPeople.ToString();
        swapAnimationPeople = false;
    }

    private void FixedUpdate()
    {
        PeopleManager();
    }
    public void PeopleManager()
    {
        if (swapAnimationPeople == false)
        {
            if (activePlusPeople)
            {
                animator.SetBool("boolPeople", true);
                timer -= Time.fixedDeltaTime;
                buttonPlusPeople.interactable = false;
                if (timer <= 0)
                {
                    wacherSourse.Stop();
                    activePlusPeople = false;
                    animator.SetBool("boolPeople", false);
                    numPeople = ++numPeople;
                    numPeopleTxt.text = numPeople.ToString();
                    food.numFoodTxt.text = food.numFood.ToString();
                    timer = 4f;
                    buttonPlusPeople.interactable = true;
                }
            }
        }
        if (swapAnimationPeople == true)
        {
            if (timer > 3f)
            {
                timer = 3f;
            }
            if (activePlusPeople)
            {
                animator.SetBool("boolUpdatePeople", true);
                timer -= Time.fixedDeltaTime;
                buttonUpdatePeople.interactable = false;
                if (timer <= 0)
                {
                    wacherSourse.Stop();
                    activePlusPeople = false;
                    animator.SetBool("boolUpdatePeople", false);
                    numPeople = ++numPeople;
                    numPeopleTxt.text = numPeople.ToString();
                    timer = 3f;
                    buttonPlusPeople.interactable = true;
                }
            }
        }
    }
    public void ClickPlusPeople()
    {
        wacherSourse.Play();
        food.numFood -= 1;
        food.numFoodTxt.text = food.numFood.ToString();
        activePlusPeople = true;
    }

    public void ClickUpdatePeople()
    {
        buttonUpdatePeople.interactable = false;
        swapAnimationPeople = true;
        food.numFood -= 5;
        food.numFoodTxt.text = food.numFood.ToString();
    }
}