using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class WatcherFood : MonoBehaviour
{
    public WatcherPeople people;
    public WatcherWarriors warriors;
    public AudioSource wacherSourse;
    private Animator animator;
    public Button buttonPlusFood;
    public Button buttonUpdateFood;
    [NonSerialized] public bool activePlusFood;
    [NonSerialized] public bool swapAnimationFood;
    [NonSerialized] public float timer=3f;
    [NonSerialized] public float numFood = 3f;
    public Text numFoodTxt;
    void Start()
    {
        animator = GetComponent<Animator>();
        numFoodTxt.text = numFood.ToString();
    }

    private void FixedUpdate()
    {
        FoodManager();
    }
    private void FoodManager()
    {
        if (swapAnimationFood == false)
        {
            if (activePlusFood)
            {
                animator.SetBool("boolFood", true);
                timer -= Time.fixedDeltaTime;
                buttonPlusFood.interactable = false;
                if (timer <= 0)
                {
                    wacherSourse.Stop();
                    activePlusFood = false;
                    animator.SetBool("boolFood", false);
                    numFood = ++numFood;
                    numFoodTxt.text = numFood.ToString();
                    people.numPeople += 1;
                    people.numPeopleTxt.text = people.numPeople.ToString();
                    timer = 3f;
                    buttonPlusFood.interactable = true;

                }
            }
        }
        if (swapAnimationFood == true)
        {
            if (timer > 2f)
            {
                timer = 2f;
            }
            if (activePlusFood)
            {
                animator.SetBool("boolUpdateFood", true);
                timer -= Time.fixedDeltaTime;
                buttonPlusFood.interactable = false;
                if (timer <= 0)
                {
                    activePlusFood = false;
                    wacherSourse.Stop();
                    animator.SetBool("boolUpdateFood", false);
                    numFood = ++numFood;
                    numFoodTxt.text = numFood.ToString();
                    people.numPeople += 1;
                    people.numPeopleTxt.text = people.numPeople.ToString();
                    timer = 2f;
                    buttonPlusFood.interactable = true;

                }
            }
        }
    }
    public void ClickPlusFood()
    {
        wacherSourse.Play();
        people.numPeople -= 1;
        people.numPeopleTxt.text = people.numPeople.ToString();
        activePlusFood = true;
    }

    public void ClickUpdateFood()
    {
        buttonUpdateFood.interactable = false;
        swapAnimationFood = true;
        people.numPeople -= 4;
        people.numPeopleTxt.text = people.numPeople.ToString();
    }
}