using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwapMusic : MonoBehaviour
{
    public Canvas pauseCanvas;
    public Button startGame;
    public Button endGame;
    [SerializeField] private AudioSource SourseWacherClickPeopel;
    [SerializeField] private AudioSource SourseWacherClickFood;
    [SerializeField] private AudioSource SourseWacherClickWarriors;
    public AudioSource menuMusic;
    public AudioSource gameMusic;
    public AudioSource backgroundMusic;

    private void Start()
    {
        menuMusic.Play();
    }
    public void PlayGameMusic()
    {
        menuMusic.Stop();
        gameMusic.Play();
        backgroundMusic.Play();
    }
    public void PlayMenuMusic()
    {
        gameMusic.Stop();
        backgroundMusic.Stop();
        menuMusic.Play();
    }
    public void PauseCanvasSwapMusic()
    {
        SourseWacherClickFood.Stop();
        SourseWacherClickFood.Stop();
        SourseWacherClickWarriors.Stop();
    }
}
